package cloudfront

import (
	"net/url"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// WebLog represents the web distribution log
type WebLog struct {
	Date                    string `json:"date"`
	Time                    string `json:"time"`
	XEdgeLocation           string `json:"x_edge_location"`
	ScBytes                 int64  `json:"sc_bytes"`
	CIP                     string `json:"c_ip"`
	CsMethod                string `json:"cs_method"`
	CsHost                  string `json:"cs_host"`
	CsURIStem               string `json:"cs_uri_stem"`
	ScStatus                int64  `json:"sc_status"`
	CsReferer               string `json:"cs_referer"`
	CsUserAgent             string `json:"cs_user_agent"`
	CsURIQuery              string `json:"cs_uri_query"`
	CsCookie                string `json:"cs_cookie"`
	XEdgeResultType         string `json:"x_edge_result_type"`
	XEdgeRequestID          string `json:"x_edge_request_id"`
	XHostHeader             string `json:"x_host_header"`
	CsProtocol              string `json:"cs_protocol"`
	CsBytes                 int64  `json:"cs_bytes"`
	TimeTaken               int64  `json:"time_taken"`
	XForwardedFor           string `json:"x_forwarded_for"`
	SslProtocol             string `json:"ssl_protocol"`
	SslCipher               string `json:"ssl_cipher"`
	XEdgeResponseResultType string `json:"x_edge_response_result_type"`
	CsProtocolVersion       string `json:"cs_protocol_version"`
	FleStatus               string `json:"fle_status"`
	FleEncryptedFields      string `json:"fle_encrypted_fields"`
	CPort                   int64  `json:"c_port"`
	TimeToFirstByte         int64  `json:"time_to_first_byte"`
	XEdgeDetailedResultType string `json:"x_edge_detailed_result_type"`
	ScContentType           string `json:"sc_content_type"`
	ScContentLen            int64  `json:"sc_content_len"`
	ScRangeStart            int64  `json:"sc_range_start"`
	ScRangeEnd              int64  `json:"sc_range_end"`
}

// S3Path represents the S3 path
// File represents the CloudFront log file name
// <optional prefix>/<distribution ID>.YYYY-MM-DD-HH.unique-ID.gz
type S3Path struct {
	Prefix         string    `json:"prefix"`
	DistributionID string    `json:"distribution_id"`
	UniqueID       string    `json:"unique_id"`
	Date           time.Time `json:"date"`
}

// ParseWebLog parses the provided log line
func ParseWebLog(line string) (l *WebLog, err error) {
	l = &WebLog{}

	splitRegex := regexp.MustCompile(`\s+`)
	fields := splitRegex.Split(line, -1)

	l.Date = fields[0]
	l.Time = fields[1]
	l.XEdgeLocation = fields[2]

	if i, err := strconv.ParseInt(fields[3], 10, 64); err != nil {
		l.ScBytes = 0
	} else {
		l.ScBytes = i
	}

	l.CIP = fields[4]
	l.CsMethod = fields[5]
	l.CsHost = fields[6]

	if decoded, err := url.QueryUnescape(fields[7]); err != nil {
		return nil, err
	} else {
		l.CsURIStem = decoded
	}

	if i, err := strconv.ParseInt(fields[8], 10, 64); err != nil {
		l.ScStatus = 0
	} else {
		l.ScStatus = i
	}

	l.CsReferer = fields[9]

	if decoded, err := url.QueryUnescape(fields[10]); err != nil {
		return nil, err
	} else {
		l.CsUserAgent = decoded
	}

	l.CsURIQuery = fields[11]
	l.CsCookie = fields[12]
	l.XEdgeResultType = fields[13]
	l.XEdgeRequestID = fields[14]
	l.XHostHeader = fields[15]
	l.CsProtocol = fields[16]

	if i, err := strconv.ParseInt(fields[17], 10, 64); err != nil {
		l.CsBytes = 0
	} else {
		l.CsBytes = i
	}

	if i, err := strconv.ParseInt(fields[18], 10, 64); err != nil {
		l.TimeTaken = 0
	} else {
		l.TimeTaken = i
	}

	l.XForwardedFor = fields[19]
	l.SslProtocol = fields[20]
	l.SslCipher = fields[21]
	l.XEdgeResponseResultType = fields[22]
	l.CsProtocolVersion = fields[23]
	l.FleStatus = fields[24]
	l.FleEncryptedFields = fields[25]

	if i, err := strconv.ParseInt(fields[26], 10, 64); err != nil {
		l.CPort = 0
	} else {
		l.CPort = i
	}

	if i, err := strconv.ParseInt(fields[27], 10, 64); err != nil {
		l.TimeToFirstByte = 0
	} else {
		l.TimeToFirstByte = i
	}

	l.XEdgeDetailedResultType = fields[28]
	l.ScContentType = fields[29]

	if i, err := strconv.ParseInt(fields[30], 10, 64); err != nil {
		l.ScContentLen = 0
	} else {
		l.ScContentLen = i
	}

	if i, err := strconv.ParseInt(fields[31], 10, 64); err != nil {
		l.ScRangeStart = 0
	} else {
		l.ScRangeStart = i
	}

	if i, err := strconv.ParseInt(fields[32], 10, 64); err != nil {
		l.ScRangeEnd = 0
	} else {
		l.ScRangeEnd = i
	}

	return
}

// ParseS3Path parses the S3 log file path
func ParseS3Path(path string) (*S3Path, error) {
	// path example: example-prefix/EMLARXS9EXAMPLE.2019-11-14-20.RT4KCN4SGK9.gz
	// parses to: <optional prefix>/<distribution ID>.YYYY-MM-DD-HH.unique-ID.gz

	f := S3Path{}
	f.Prefix = filepath.Dir(path)

	file := strings.TrimLeft(path[len(f.Prefix):], "/")
	p := strings.Split(file[:len(file)-len(filepath.Ext(file))], ".")
	f.DistributionID = p[0]
	f.UniqueID = p[2]

	t, err := time.Parse("2006-01-02-15", p[1])

	if err != nil {
		return nil, err
	}

	f.Date = t

	return &f, nil
}
