# AWS Log Parsers

Parse common AWS log formats.

Supported formats:

- CloudFront access logs:

  ```go
  package main

  import (
    "fmt"

    "gitlab.com/czerasz/go-aws-log-parsers/cloudfront"
  )

  func main() {
    line := "2019-12-04	21:02:31	LAX1	392	192.0.2.100	GET	d111111abcdef8.cloudfront.net	/index.html	200	-	Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/78.0.3904.108%20Safari/537.36	-	-	Hit	SOX4xwn4XV6Q4rgb7XiVGOHms_BGlTAC4KyHmureZmBNrjGdRLiNIQ==	d111111abcdef8.cloudfront.net	https	23	0.001	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/2.0	-	-	11040	0.001	Hit	text/html	78	-	-"
    l, err := cloudfront.ParseWebLog(line)
    if err != nil {
      panic(err)
    }
    fmt.Printf("%+v\n", l)
  }
  ```

## Test

```bash
go test ./...
```

- update golden files:

  ```bash
  go test ./cloudfront/cloudfront_test.go --update=true
  ```

## Similar Projects

- [GitHub: `stephane-martin/w3c-extendedlog-parser`](https://github.com/stephane-martin/w3c-extendedlog-parser)
- [GitHub: `zumba/cloudfront-loggly-uploader`](https://github.com/zumba/cloudfront-loggly-uploader)
