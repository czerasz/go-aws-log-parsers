package cloudfront_test

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/czerasz/go-aws-log-parsers/cloudfront"
)

var update = flag.Bool("update", false, "update .golden.json files")

var samples = `
2019-12-04	21:02:31	LAX1	392	192.0.2.100	GET	d111111abcdef8.cloudfront.net	/index.html	200	-	Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/78.0.3904.108%20Safari/537.36	-	-	Hit	SOX4xwn4XV6Q4rgb7XiVGOHms_BGlTAC4KyHmureZmBNrjGdRLiNIQ==	d111111abcdef8.cloudfront.net	https	23	0.001	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/2.0	-	-	11040	0.001	Hit	text/html	78	-	-
2019-12-04	21:02:31	LAX1	392	192.0.2.100	GET	d111111abcdef8.cloudfront.net	/index.html	200	-	Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/78.0.3904.108%20Safari/537.36	-	-	Hit	k6WGMNkEzR5BEM_SaF47gjtX9zBDO2m349OY2an0QPEaUum1ZOLrow==	d111111abcdef8.cloudfront.net	https	23	0.000	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/2.0	-	-	11040	0.000	Hit	text/html	78	-	-
2019-12-04	21:02:31	LAX1	392	192.0.2.100	GET	d111111abcdef8.cloudfront.net	/index.html	200	-	Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/78.0.3904.108%20Safari/537.36	-	-	Hit	f37nTMVvnKvV2ZSvEsivup_c2kZ7VXzYdjC-GUQZ5qNs-89BlWazbw==	d111111abcdef8.cloudfront.net	https	23	0.001	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/2.0	-	-	11040	0.001	Hit	text/html	78	-	-	
2019-12-13	22:36:27	SEA19-C1	900	192.0.2.200	GET	d111111abcdef8.cloudfront.net	/favicon.ico	502	http://www.example.com/	Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/78.0.3904.108%20Safari/537.36	-	-	Error	1pkpNfBQ39sYMnjjUQjmH2w1wdJnbHYTbag21o_3OfcQgPzdL2RSSQ==	www.example.com	http	675	0.102	-	-	-	Error	HTTP/1.1	-	-	25260	0.102	OriginDnsError	text/html	507	-	-
2019-12-13	22:36:26	SEA19-C1	900	192.0.2.200	GET	d111111abcdef8.cloudfront.net	/	502	-	Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/78.0.3904.108%20Safari/537.36	-	-	Error	3AqrZGCnF_g0-5KOvfA7c9XLcf4YGvMFSeFdIetR1N_2y8jSis8Zxg==	www.example.com	http	735	0.107	-	-	-	Error	HTTP/1.1	-	-	3802	0.107	OriginDnsError	text/html	507	-	-
2019-12-13	22:37:02	SEA19-C2	900	192.0.2.200	GET	d111111abcdef8.cloudfront.net	/	502	-	curl/7.55.1	-	-	Error	kBkDzGnceVtWHqSCqBUqtA_cEs2T3tFUBbnBNkB9El_uVRhHgcZfcw==	www.example.com	http	387	0.103	-	-	-	Error	HTTP/1.1	-	-	12644	0.103	OriginDnsError	text/html	507	-	-
`

func TestParseWebLog(t *testing.T) {
	// iterate through log lines
	for i, line := range strings.Split(strings.TrimSpace(samples), "\n") {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			t.Parallel()

			l, err := cloudfront.ParseWebLog(line)
			if err != nil {
				t.Error(err)
			}

			// JSON stringify
			result, err := json.Marshal(l)
			if err != nil {
				t.Error(err)
			}

			goldenFile := fmt.Sprintf("../testdata/parse-web-log.%d.golden.json", i)

			// If update is set, write to the golden file
			if *update {
				// create file if it doesn't exist
				f, err := os.OpenFile(goldenFile, os.O_RDONLY|os.O_CREATE, 0644)
				if err != nil {
					t.Error(err)
				}
				f.Close()
				// update file
				ioutil.WriteFile(goldenFile, result, 0644)
			}

			// read golden file
			expected, err := ioutil.ReadFile(goldenFile)
			if err != nil {
				t.Error(err)
			}

			// compare golden file with current content
			if !bytes.Equal(expected, result) {
				t.Errorf("\nwanted: %s\ngot: %s", expected, result)
			}
		})
	}
}

func TestParseS3Path(t *testing.T) {
	tests := []struct {
		name                 string
		path                 string
		wantedDistributionID string
		wantedPrefix         string
		wantedUniqueID       string
		wantedYear           int
		wantedMonth          time.Month
		wantedDay            int
		wantedHour           int
		// wantedZone int
	}{
		{
			name:                 "path with prefix",
			path:                 "/example-prefix/EMLARXS9EXAMPLE.2019-11-14-20.RT4KCN4SGK9.gz",
			wantedDistributionID: "EMLARXS9EXAMPLE",
			wantedPrefix:         "/example-prefix",
			wantedUniqueID:       "RT4KCN4SGK9",
			wantedYear:           2019,
			wantedMonth:          time.November,
			wantedDay:            14,
			wantedHour:           20,
		},
		{
			name:                 "path without prefix",
			path:                 "/EMLARXS9EXAMPLE.2019-11-14-20.RT4KCN4SGK9.gz",
			wantedDistributionID: "EMLARXS9EXAMPLE",
			wantedPrefix:         "/",
			wantedUniqueID:       "RT4KCN4SGK9",
			wantedYear:           2019,
			wantedMonth:          time.November,
			wantedDay:            14,
			wantedHour:           20,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			p, err := cloudfront.ParseS3Path(tt.path)
			if err != nil {
				t.Errorf("parsing was not successful: %s", err)
			}

			if p.DistributionID != tt.wantedDistributionID {
				t.Errorf("wrong DistributionID, wanted: %s, got: %s", tt.wantedDistributionID, p.DistributionID)
			}
			if p.Prefix != tt.wantedPrefix {
				t.Errorf("wrong Prefix, wanted: %s, got: %s", tt.wantedPrefix, p.Prefix)
			}
			if p.UniqueID != tt.wantedUniqueID {
				t.Errorf("wrong UniqueID, wanted: %s, got: %s", tt.wantedUniqueID, p.UniqueID)
			}
			if p.Date.Year() != tt.wantedYear {
				t.Errorf("wrong year, wanted: %d, got: %d", tt.wantedYear, p.Date.Year())
			}
			if p.Date.Month() != tt.wantedMonth {
				t.Errorf("wrong month, wanted: %d, got: %d", tt.wantedMonth, p.Date.Month())
			}
			if p.Date.Day() != tt.wantedDay {
				t.Errorf("wrong day, wanted: %d, got: %d", tt.wantedDay, p.Date.Day())
			}
			if p.Date.Hour() != tt.wantedHour {
				t.Errorf("wrong hour, wanted: %d, got: %d", tt.wantedHour, p.Date.Hour())
			}
			if p.Date.Minute() != 0 {
				t.Errorf("wrong minute, wanted: %d, got: %d", 0, p.Date.Minute())
			}
			if z, _ := p.Date.Zone(); z != time.UTC.String() {
				t.Errorf("wrong zone, wanted: %s, got: %s", time.UTC.String(), z)
			}
		})
	}
}
